import {Observable} from "rxjs";

export interface UploadResponse {
  percentage?: Observable<number> | undefined
  path: Observable<string>
  onCancel: () => {}
}
