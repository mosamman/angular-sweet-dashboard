import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpResponseBase
} from '@angular/common/http'
import {Observable, of} from 'rxjs'
import {catchError, finalize, mergeMap, tap} from 'rxjs/operators'
import {ApiError} from './api-error'
import {SearchBag} from "../../components/table/model/search-bag";
import {ApiErrorResponse} from "./api-error-response";
import {AuthError} from "./auth-error";


// noinspection JSUnusedGlobalSymbols
export class BaseApi {


  public constructor(
    public http: HttpClient
  ) {

  }

  public get<RESPONSE>(request: { url: string, searchBag?: SearchBag, token?: string }): Observable<RESPONSE> {
    let path = request.searchBag !== undefined ? `${request.url}?${BaseApi.buildQuery(request.searchBag)}` : request.url;
    return this.http.get<RESPONSE>(`${path}`, {
      observe: 'response',
      headers: BaseApi.getHeaders(request.token)
    }).pipe(
      mergeMap(BaseApi.handleResponse),
      catchError(BaseApi.handleError)
    )
  }

  public delete(request: { url: string, token?: string }): Observable<void> {
    return this.http.delete(`${request.url}`, {
      observe: 'response',
      headers: BaseApi.getHeaders(request.token)
    }).pipe(
      catchError(BaseApi.handleError)
    )
  }

  public post(request: { url: string, body: any, token?: string }): Observable<void> {
    return this.http.post(`${request.url}`, request.body, {
      observe: 'response',
      headers: BaseApi.getHeaders(request.token)
    }).pipe(
      catchError(BaseApi.handleError)
    )
  }


  public postWithResponse<RESPONSE>(request: { url: string, body: any, token?: string }): Observable<RESPONSE> {
    return this.http.post<RESPONSE>(`${request.url}`, request.body,
      {
        observe: 'response',
        headers: BaseApi.getHeaders(request.token)
      }).pipe(
      mergeMap(BaseApi.handleResponse),
      catchError(BaseApi.handleError)
    )
  }

  public put(request: { url: string, body: any, token?: string }): Observable<void> {
    return this.http.put(`${request.url}`, request.body, {
      observe: 'response',
      headers: BaseApi.getHeaders(request.token)
    }).pipe(
      catchError(BaseApi.handleError)
    )
  }


  public putWithResponse<RESPONSE>(request: { url: string, body: any, token?: string }): Observable<RESPONSE> {
    return this.http.put<RESPONSE>(`${request.url}`, request.body,
      {
        observe: 'response',
        headers: BaseApi.getHeaders(request.token)
      }).pipe(
      mergeMap(BaseApi.handleResponse),
      catchError(BaseApi.handleError)
    )
  }

  public buildPath(request: { url: string, searchBag?: SearchBag, token?: string }): string {
    let url = `${request.url}`

    if (request.searchBag !== undefined) {
      url = `${url}?${BaseApi.buildQuery(request.searchBag)}`
      if (request.token == undefined) {
        return `${url}&token=${request.token}`
      } else {
        return url
      }
    } else {
      if (request.token == undefined) {
        return `${url}?token=${request.token}`
      } else {
        return url
      }
    }
  }

  private static getHeaders(token: string | undefined): HttpHeaders {
    if (token !== undefined) {
      return new HttpHeaders({'Content-Type': 'application/json', 'Authorization': `Bearer ${token}`})
    } else {
      return new HttpHeaders({'Content-Type': 'application/json'})
    }
  }


  private static handleResponse<RESPONSE>(response: HttpResponse<RESPONSE>): Observable<RESPONSE> {
    if (response.body != null)
      return of(response.body)
    else
      throw new ApiError(500, 'client/parsing')
  }

  private static handleError<RESPONSE>(error: HttpErrorResponse): Observable<any> {
    if (error.status == CLINT_ERROR)
      throw new ApiError(500, 'client/general', "Something went wrong.")
    else if (error.status == INTERNET_ERROR)
      throw new ApiError(500, 'client/connection', "No Internet Connection.")
    else if (error.status == 401) {
      let errorResponse: ApiErrorResponse = error.error
      throw new AuthError(errorResponse.code, errorResponse.message)
    }
    else {
      let errorResponse: ApiErrorResponse = error.error
      throw new ApiError(error.status, errorResponse.code, errorResponse.message)
    }
  }


  private static buildQuery(bag: SearchBag): String {

    let filters: string = bag.filters?.map(filter => `${filter.id}|${filter.operator}|${filter.value}|${filter.type}`).join(',') ?? ''

    let sorts: string = bag?.sorts?.map(sort => `${sort.sortField}|${sort.sortDirection}`).join(',') ?? ''

    let page = bag?.pageBox?.page ?? ''
    let pageSize = bag?.pageBox?.pageSize ?? ''

    return `page=${page}&pageSize=${pageSize}&filters=${filters}&sorts=${sorts}`
  }
}


export class ConnectionIntercept implements HttpInterceptor {
  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!window.navigator.onLine) {
      const error = {
        status: INTERNET_ERROR,
        error: {
          description: 'Check Connectivity!'
        },
        statusText: 'Check Connectivity!'
      };
      throw new HttpErrorResponse(error);
    } else {
      return next.handle(request);
    }
  }
}

export class LoggingIntercept implements HttpInterceptor {
  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const startTime = Date.now();

    let response: HttpResponseBase | null = null

    return next.handle(request).pipe(
      tap({
        next: event => {
          if (event instanceof HttpResponse) {
            response = event
          } else {
            response = null
          }
        },
        error: error => response = error
      }),
      finalize(() => {
        LoggingIntercept.logRequest(startTime, request, response);
      })
    );

  }

  private static logRequest(startTime: number, request: HttpRequest<any>, response: HttpResponseBase | null) {
    const elapsedTime = Date.now() - startTime
    let message = `${response?.status} ${request.method} ${request.urlWithParams} in ${elapsedTime} ms`

    if (request.method.toUpperCase() === "POST") {
      message += `\n body : ${JSON.stringify(request.body)}`
    }

    if (response instanceof HttpErrorResponse) {
      message += `\n error : ${response.error}`
    } else if (response instanceof HttpResponse) {
      message += `\n response : ${JSON.stringify(response.body)}`
    }

    console.log(message)
  }
}


export const httpInterceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: LoggingIntercept, multi: true},
  {provide: HTTP_INTERCEPTORS, useClass: ConnectionIntercept, multi: true},
];


const CLINT_ERROR = 0
const INTERNET_ERROR = 5555
