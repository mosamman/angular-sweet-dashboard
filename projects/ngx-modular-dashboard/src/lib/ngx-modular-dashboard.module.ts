import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {HttpClientModule} from "@angular/common/http";
import {FlexLayoutModule} from "@angular/flex-layout";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatChipsModule} from "@angular/material/chips";
import {MatSortModule} from "@angular/material/sort";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatSelectModule} from "@angular/material/select";
import {MatDialogModule} from "@angular/material/dialog";
import {MatMultiSortModule} from "ngx-mat-multi-sort";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatExpansionModule} from "@angular/material/expansion";
import {CommonModule, DatePipe} from "@angular/common";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from "@angular/material-moment-adapter";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatMenuModule} from "@angular/material/menu";
import {LottieModule} from "ngx-lottie";
import player from 'lottie-web';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatListModule} from "@angular/material/list";
import {NgScrollbarModule} from "ngx-scrollbar";
import {InternalFormComponent} from "./components/form/components/internal-form.component";
import {TableComponent} from "./components/table/table.component";
import {NotFoundComponent} from "./components/notfound/not-found.component";
import {DIR_DOCUMENT} from "@angular/cdk/bidi";
import {httpInterceptorProviders} from "./infrastructure/api/base.api";
import {DialogFormComponent} from './components/form/dialog-form.component';
import {LoadingBox} from "./components/loading/loading.component";
import {TypeInputComponent} from "./components/form/components/type.component";
import {SelectInputComponent} from "./components/form/components/select.component";
import {AsyncSelectInputComponent} from "./components/form/components/async-select.component";
import {DateInputComponent} from "./components/form/components/date.component";
import {TextareaInputComponent} from "./components/form/components/text-area.component";
import {FileInputComponent} from "./components/form/components/file.component";
import {AutocompleteInputComponent} from "./components/form/components/auto-complete-select.component";
import {PlainColumn, PlainColumnContentDirective} from "./components/table/components/columns/plain-column.component";
import {ListColumn} from "./components/table/components/columns/list-column.component";
import {ActionsColumn} from "./components/table/components/columns/action-column.component";
import {TypeFilter} from "./components/table/components/filters/type-filter.component";
import {DateFilter} from "./components/table/components/filters/date-filter.component";
import {SelectFilter} from "./components/table/components/filters/select-filter.component";
import {AsyncSelectFilter} from "./components/table/components/filters/async-select-filter.component";
import {DetailsComponent} from "./components/details/details.component";
import {EmbeddedFormComponent} from "./components/form/embedded-form.component";
import {NewPasswordComponent} from "./components/form/components/new-password.component";
import {PasswordComponent} from "./components/form/components/password.component";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {ResizedDirective} from "./components/table/resize.directive";
import {CheckBoxComponent} from "./components/form/components/check-box.component";
import {
  InlineDetailsFieldComponent
} from "./components/details/components/inline-details-field/inline-details-field.component";
import {
  AboveDetailsFieldComponent
} from "./components/details/components/above-details-field/above-details-field.component";
import {SliderComponent} from "./components/form/components/slider.component";
import {MatSliderModule} from "@angular/material/slider";
import {RangeSliderComponent} from "./components/form/components/range-slider.component";
import {NgxSliderModule} from "@angular-slider/ngx-slider";





export function playerFactory() {
  return player;
}



@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    ResizedDirective,
    NotFoundComponent,

    EmbeddedFormComponent,
    DialogFormComponent,
    InternalFormComponent,
    TypeInputComponent,
    SelectInputComponent,
    SliderComponent,
    RangeSliderComponent,
    AsyncSelectInputComponent,
    DateInputComponent,
    TextareaInputComponent,
    FileInputComponent,
    AutocompleteInputComponent,
    NewPasswordComponent,
    PasswordComponent,
    CheckBoxComponent,

    TableComponent,
    PlainColumn,
    ListColumn,
    ActionsColumn,
    TypeFilter,
    DateFilter,
    SelectFilter,
    AsyncSelectFilter,
    DetailsComponent,
    LoadingBox,
    InlineDetailsFieldComponent,
    AboveDetailsFieldComponent,

    PlainColumnContentDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatChipsModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatDialogModule,
    MatMultiSortModule,
    CommonModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatTooltipModule,
    MatMenuModule,
    MatListModule,
    LottieModule.forRoot({player: playerFactory}),
    MatCheckboxModule,
    NgScrollbarModule,
    MatProgressBarModule,
    MatSliderModule,
    NgxSliderModule,

  ],
  providers: [
    httpInterceptorProviders,
    DatePipe,
    {provide: DIR_DOCUMENT, useValue: 'ltr'},
    {provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {useUtc: true}},
  ],
  exports: [
    DetailsComponent,
    NotFoundComponent,
    DialogFormComponent,
    EmbeddedFormComponent,
    TableComponent,
    LoadingBox
  ]
})
export class NgxModularDashboardModule {


}
