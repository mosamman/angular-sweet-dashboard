import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {DetailsStructure} from "./model/details-structure";
import {FieldViewStructure} from "./model/field-structure";
import {Subscription} from "rxjs";
import {FieldStyle} from "./model/field-style";
import {FlexDefinition} from "../common/flex-definition";
import {FlexFactory} from "../common/flex-factory";
import {Dimensions} from "../common/dimensions";
import {FieldType} from "./model/field-type";


@Component({
  selector: 'ng-modular-details',
  styleUrls: ['./details.component.scss', '../header.scss'],
  template: `
    <div class="header">
      <ng-container *ngIf="structure.style.showHeader === true">
        <div fxLayout="row">
          <div class="title" fxFlex="none">{{structure.style.title}}</div>
          <div class="button-bar" fxFlex="grow">
            <ng-container *ngIf="structure.headerActions != undefined">
              <ng-container *ngFor="let action of structure.headerActions">
                <ng-container *ngIf="action.display() === true">
                  <button mat-stroked-button color="primary" (click)="action.callback(data)">
                    <ng-container *ngIf="action.icon !== null">
                      <mat-icon class="with-text">{{action.icon}}</mat-icon>
                      {{action.name}}
                    </ng-container>
                  </button>
                </ng-container>
              </ng-container>
            </ng-container>
          </div>
        </div>
      </ng-container>

      <div class="mat-elevation-z{{structure.style.elevation}} info-card">
        <ng-modular-loading [loading]="loading"
                            [empty]="loading === false && data === null"
                            [loadingAnimation]="structure.style.loadingAnimation"
                            [emptyImage]="structure.style.emptyArt?.art"
                            [emptyText]="structure.style.emptyArt?.text">
        </ng-modular-loading>

        <ng-container *ngIf="loading === false && data !== null">
          <div fxLayout="row wrap" class="info-card-wrapper">
            <ng-container *ngFor="let field of structure.fields">

              <ng-container *ngIf="dimensions(field.dimensions).width !== undefined else FLEX">
                <ng-container *ngIf="field.type.toString() === FieldType.TITLE.toString() else DATA">
                    <div class="input-box section-title" fxFlex="100">{{field.title}}</div>
                </ng-container>

                <ng-template #DATA [ngSwitch]="structure.style.fieldStyle.toString()">

                  <ng-modular-inline-details-field
                    *ngSwitchCase="FieldStyle.INLINE.toString()"
                    [title]="field.title"
                    [type]="field.type"
                    [value]="getValue(field)"
                    class="info-box-container"
                    fxFlex.lt-md="100"
                    [ngStyle]="{'width.px': dimensions(field.dimensions).width}"
                    [ngStyle.lt-md]="{'width.px': null}">
                  </ng-modular-inline-details-field>

                  <ng-modular-above-details-field
                    *ngSwitchCase="FieldStyle.ABOVE.toString()"
                    [title]="field.title"
                    [type]="field.type"
                    [value]="getValue(field)"
                    class="info-box-container"
                    fxFlex.lt-md="100"
                    [ngStyle]="{'width.px': dimensions(field.dimensions).width}"
                    [ngStyle.lt-md]="{'width.px': null}">
                  </ng-modular-above-details-field>

                </ng-template>
              </ng-container>

              <ng-template #FLEX>
                <ng-container *ngIf="field.type.toString() === FieldType.TITLE.toString() else DATA">
                  <div class="input-box section-title" fxFlex="100">{{field.title}}</div>
                </ng-container>


                <ng-template #DATA [ngSwitch]="structure.style.fieldStyle.toString()">
                  <ng-modular-inline-details-field
                    *ngSwitchCase="FieldStyle.INLINE.toString()"
                    [title]="field.title"
                    [type]="field.type"
                    [value]="getValue(field)"
                    class="info-box-container"
                    fxFlex.gt-md="1 1 {{flex(field.dimensions).lg}}%"
                    fxFlex.md="1 1 {{flex(field.dimensions).md}}%"
                    fxFlex.sm="1 1 {{flex(field.dimensions).sm}}%"
                    fxFlex.xs="1 1 {{flex(field.dimensions).xs}}%">
                  </ng-modular-inline-details-field>

                  <ng-modular-above-details-field
                    *ngSwitchCase="FieldStyle.ABOVE.toString()"
                    [title]="field.title"
                    [type]="field.type"
                    [value]="getValue(field)"
                    class="info-box-container"
                    fxFlex.gt-md="1 1 {{flex(field.dimensions).lg}}%"
                    fxFlex.md="1 1 {{flex(field.dimensions).md}}%"
                    fxFlex.sm="1 1 {{flex(field.dimensions).sm}}%"
                    fxFlex.xs="1 1 {{flex(field.dimensions).xs}}%">
                  </ng-modular-above-details-field>

                </ng-template>
              </ng-template>

            </ng-container>
          </div>
        </ng-container>

      </div>
    </div>
  `,
})
export class DetailsComponent implements OnInit, OnDestroy {

  FieldStyle = FieldStyle
  FieldType = FieldType
  loading: boolean = true
  data: object | null = null
  //TODO implement error view and retry action


  @Input() structure!: DetailsStructure

  private detailsSubscription: Subscription | null = null
  private reloadSubscription: Subscription | null = null

  ngOnInit(): void {
    this.reload();
    this.reloadSubscription = this.structure.reloadEvent?.subscribe(_ => this.reload()) ?? null
  }


  getValue(field: FieldViewStructure): string | number | boolean {
    if (this.data != null) {
      return field.transform != undefined ? field.transform(this.data) : Object.entries(this.data).find((entry) => entry[0] === field.id)![1]
    } else
      return ''
  }

  private reload() {
    this.detailsSubscription = this.structure.loadData().subscribe({
      next: data => {
        this.loading = false
        this.data = data
      },
      error: error => {
        this.loading = false
        this.data = null
        console.log(error)
      }
    })
  }

  ngOnDestroy(): void {
    this.detailsSubscription?.unsubscribe()
    this.reloadSubscription?.unsubscribe()
  }

  dimensions(dimensions: Dimensions) {
    return this.structure.style.dimensions !== undefined ? this.structure.style.dimensions : dimensions
  }


  flex(dimensions: Dimensions): FlexDefinition {
    if (dimensions.flex !== undefined) {
      return dimensions.flex
    } else if (this.structure.style.dimensions !== undefined && this.structure.style.dimensions.flex !== undefined) {
      return this.structure.style.dimensions.flex
    } else {
      return FlexFactory.defaultFlex().flex!
    }
  }
}
