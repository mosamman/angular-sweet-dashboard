import {Observable} from "rxjs";
import {FieldViewStructure} from "./field-structure";
import {DetailsHeaderActionViewStructure} from "./details-header-action-structure";
import {DetailsStyle} from "./details-style";


export interface DetailsStructure {
  reloadEvent?: Observable<void>
  headerActions?: DetailsHeaderActionViewStructure[]
  style: DetailsStyle
  loadData: () => Observable<any>
  fields: FieldViewStructure[]
}
