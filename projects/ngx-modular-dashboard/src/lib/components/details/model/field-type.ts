export enum FieldType {
  VALUE = 'VALUE',
  TEXT = 'TEXT',
  TITLE = 'TITLE',
}
