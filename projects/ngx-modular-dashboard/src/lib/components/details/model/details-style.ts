import {Art} from "../../common/art";
import {FieldStyle} from "./field-style";
import {Dimensions} from "../../common/dimensions";

export interface DetailsStyle {
  loadingAnimation?: string

  emptyArt?: Art
  errorArt?: Art

  title: string
  showHeader?: boolean
  elevation: number

  fieldStyle: FieldStyle
  dimensions?: Dimensions
}
