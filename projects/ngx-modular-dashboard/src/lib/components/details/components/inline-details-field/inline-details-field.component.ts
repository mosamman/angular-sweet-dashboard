import {Component, Input, OnInit} from '@angular/core';
import {FieldType} from "../../model/field-type";

@Component({
  selector: 'ng-modular-inline-details-field',
  templateUrl: './inline-details-field.component.html',
  styleUrls: ['./inline-details-field.component.scss']
})
export class InlineDetailsFieldComponent implements OnInit {

  FieldType = FieldType

  @Input() title !: string
  @Input() value !: string | number | boolean

  @Input() type !: FieldType

  constructor() {
  }

  ngOnInit(): void {
  }

}
