import {Component, Input, OnInit} from '@angular/core';
import {FieldType} from "../../model/field-type";

@Component({
  selector: 'ng-modular-above-details-field',
  templateUrl: './above-details-field.component.html',
  styleUrls: ['./above-details-field.component.scss']
})
export class AboveDetailsFieldComponent implements OnInit {

  FieldType = FieldType

  @Input() title !: string
  @Input() value !: string | number | boolean

  @Input() type !: FieldType

  constructor() {
  }

  ngOnInit(): void {
  }

}
