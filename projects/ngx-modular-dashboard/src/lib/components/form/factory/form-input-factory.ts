import {AbstractControl, ValidationErrors, Validators} from "@angular/forms";
import {
  AsyncSelectInputStructure, CheckBoxInputStructure,
  DateInputStructure, FileInputStructure, NewPasswordInputStructure, PasswordInputStructure, RangeSlideInputStructure,
  SearchInputStructure,
  SelectInputStructure, SlideInputStructure,
  TypeInputStructure
} from "../model/form-input-structure";
import {InputViewStructure} from "../model/input-structure";
import {Subject} from "rxjs";
import {SearchResult} from "../model/search-result";
import {InputType} from "../model/enums/input-type";
import {InputValidator} from "../model/input-validator";
import {FlexFactory} from "../../common/flex-factory";

// noinspection JSUnusedGlobalSymbols
export class FormInputFactory {

  private static required(field: string): InputValidator {
    return {
      id: 'required',
      message: `${field} is required`,
      validatorFun: Validators.required
    }
  }

  private static requiredTrue(field: string): InputValidator {
    return {
      id: 'required',
      message: `${field} is required`,
      validatorFun: Validators.requiredTrue
    }
  }

  public static digitsValidator: InputValidator = {
    id: 'pattern',
    message: 'Only numbers from 0 to 9 allowed',
    validatorFun: Validators.pattern("^[0-9.]+$")
  }

  static passwordMatchValidator(newPasswordInputId: string): InputValidator {
    return {
      id: 'password_not_matching',
      message: 'Password Not Matched',
      validatorFun: (control: AbstractControl): ValidationErrors | null => {
        const passwordControl = control.get(newPasswordInputId);
        const confirmControl = control.get(`confirm_${newPasswordInputId}`);

        return passwordControl && confirmControl && passwordControl.value !== confirmControl.value ? {password_not_matching: true} : null;
      }
    }
  }

  static passwordMatchValidatorRef(): InputValidator {
    return {
      id: 'password_not_matching',
      message: 'Password Not Matched'
    }
  }

  public static type(structure: TypeInputStructure): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let required = structure.required === true ? [FormInputFactory.required(fieldName)] : []
    let validators = required.concat(structure.validators ?? [])

    return {
      id: structure.id,
      value: structure.value ?? null,
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: validators,
      display: structure.display ?? defaultDisplay,
      type: structure.type,
    }
  }

  public static search(structure: SearchInputStructure): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let required = structure.required === true ? [FormInputFactory.required(fieldName)] : []
    let defaultAutoComplete = () => new Subject<SearchResult[]>().asObservable()

    return {
      id: structure.id,
      value: structure.value ?? null,
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: required,
      display: structure.display ?? defaultDisplay,
      autoComplete: structure.autoComplete ?? defaultAutoComplete,
      type: InputType.AUTO_COMPLETE,
    }
  }

  public static select(structure: SelectInputStructure): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let validators = structure.required === true ? [FormInputFactory.required(fieldName)] : []

    return {
      id: structure.id,
      value: structure.value ?? null,
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: validators,
      display: structure.display ?? defaultDisplay,
      type: InputType.SELECT,
      list: structure.list,
      multiSelect: structure.multiSelect ?? false
    }
  }

  public static asyncSelect(structure: AsyncSelectInputStructure): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let validators = structure.required === true ? [FormInputFactory.required(fieldName)] : []

    return {
      id: structure.id,
      value: structure.value ?? null,
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: validators,
      display: structure.display ?? defaultDisplay,
      type: InputType.ASYNC_SELECT,
      asyncList: structure.asyncList
    }
  }

  public static date(structure: DateInputStructure): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let validators = structure.required === true ? [FormInputFactory.required(fieldName)] : []

    return {
      id: structure.id,
      value: structure.value ?? null,
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: validators,
      display: structure.display ?? defaultDisplay,
      type: InputType.DATE,
      minDate: structure.minDate,
      maxDate: structure.maxDate
    }
  }

  public static file(structure: FileInputStructure<any>): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let validators = structure.required === true ? [FormInputFactory.required(fieldName)] : []

    return {
      id: structure.id,
      value: structure.value ?? null,
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: validators,
      display: structure.display ?? defaultDisplay,
      type: InputType.FILE,
      fileServer: structure.fileServer,
      fileType: structure.fileType
    }
  }

  public static password(structure: PasswordInputStructure): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let required = structure.required === true ? [FormInputFactory.required(fieldName)] : []
    let validators = required.concat(structure.validators ?? [])

    return {
      id: structure.id,
      value: structure.value ?? null,
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: validators,
      display: structure.display ?? defaultDisplay,
      type: InputType.PASSWORD,
      forgetPassword: structure.forgetPassword,
      showForgetPassword: structure.showForgetPassword
    }
  }

  public static newPassword(structure: NewPasswordInputStructure): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let required = structure.required === true ? [FormInputFactory.required(fieldName)] : []
    let validators = required.concat(structure.validators ?? [])

    return {
      id: structure.id,
      value: structure.value ?? null,
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: validators,
      display: structure.display ?? defaultDisplay,
      type: InputType.NEW_PASSWORD
    }
  }

  public static checkBox(structure: CheckBoxInputStructure): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let validators = structure.required === true ? [FormInputFactory.requiredTrue(fieldName)] : []

    return {
      id: structure.id,
      value: structure.value === true,
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: validators,
      display: structure.display ?? defaultDisplay,
      type: InputType.CHECK_BOX
    }
  }


  public static slider(structure: SlideInputStructure): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let validators = structure.required === true ? [FormInputFactory.requiredTrue(fieldName)] : []

    return {
      id: structure.id,
      value: structure.value ?? null,
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: validators,
      display: structure.display ?? defaultDisplay,
      type: InputType.SLIDER,
      minLimit: structure.minLimit,
      maxLimit: structure.maxLimit,
    }
  }


  public static rangeSlider(structure: RangeSlideInputStructure): InputViewStructure {
    let defaultDisplay = () => true
    const fieldName = structure.label.replace("*", "").trim()
    let validators = structure.required === true ? [FormInputFactory.requiredTrue(fieldName)] : []

    return {
      id: structure.id,
      value: (<string | null | undefined>structure.value)?.split(",").map(v => Number(v)) ?? [structure.minLimit, structure.maxLimit],
      label: structure.label,
      longLabel: structure.longLabel ? structure.longLabel : structure.label,
      dimensions: structure.dimensions ?? FlexFactory.fixedFlex(100),
      validators: validators,
      display: structure.display ?? defaultDisplay,
      type: InputType.RANGE_SLIDER,
      minLimit: structure.minLimit,
      maxLimit: structure.maxLimit,
    }
  }

  private static emptyRangeValue(minLimit: number, maxLimit: number): number[] {
    return [minLimit, maxLimit]
  }


  public static title(title: string): InputViewStructure {
    return {
      id: '',
      value: title,
      label: title,
      longLabel: title,
      dimensions: FlexFactory.fixedFlex(100),
      validators: [],
      display: () => true,
      type: InputType.TITLE,
    }
  }


}
