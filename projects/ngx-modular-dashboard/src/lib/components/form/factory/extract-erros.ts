import {FormGroup, ValidationErrors} from "@angular/forms";
import {InputValidator} from "../model/input-validator";
import {InputViewStructure} from "../model/input-structure";

export class FormExtractErrors {

  public static extract(request :{form: FormGroup, crossValidators: InputValidator[], inputs: InputViewStructure[]}): string[] {
    const errors: string[] = []

    const formErrors: ValidationErrors | undefined | null = request.form.errors;

    if (formErrors !== null && formErrors !== undefined) {
      Object.keys(formErrors).forEach(keyError => {
        const error = request.crossValidators.find(validator => validator.id === keyError)?.message
        if (error) errors.push(error)
      });
    }

    Object.keys(request.form.controls).forEach(controlKey => {
      const controlErrors: ValidationErrors | undefined | null = request.form.controls[controlKey]?.errors;
      if (controlErrors !== null && controlErrors !== undefined) {
        Object.keys(controlErrors).forEach(keyError => {
          const error = request.inputs.find(
            input => input.id === controlKey
          )?.validators.find(
            validator => validator.id === keyError
          )?.message
          if (error) errors.push(error)
        });
      }
    });
    return errors;
  }
}
