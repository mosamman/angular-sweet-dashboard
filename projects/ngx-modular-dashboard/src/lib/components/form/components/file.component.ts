import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {InputViewStructure} from "../model/input-structure";
import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn} from "@angular/forms";
import {FormStyle} from "../model/form-style";
import {ApiError} from "../../../infrastructure/api/api-error";
import {Subscription} from "rxjs";
import {UploadResponse} from "../../../infrastructure/file-server/upload-response";
import {BaseInput} from "./base-input-component";

@Component({
  selector: 'ng-modular-file-input',
  styleUrls: ['./file.component.scss'],
  template: `
    <div fxFill class="file-input">
      <mat-form-field ngDefaultControl [id]="input.id" [formControl]="control">
        <input matInput type="text" [formControl]="control" [value]="path">
      </mat-form-field>

      <input type="file" [name]="input.id" [accept]="input.fileType" #fileInput
             (change)="onChange(fileInput.files);fileInput.onreset;">


      <div class="file-input-box">

        <div class="label-box">
          {{fileMessage}}
        </div>


        <ng-container *ngIf="uploading === false">
          <div class="action-button" (click)="fileInput.value = '';fileInput.click()">
            <mat-icon color="primary">upload</mat-icon>
          </div>
        </ng-container>

        <ng-container *ngIf="uploading === true">
          <div class="action-button" (click)="cancel()">
            <mat-icon color="primary">highlight_off</mat-icon>
          </div>

          <div class="progress-indicator">
            <mat-progress-bar mode="determinate" [value]="progress"></mat-progress-bar>
          </div>
        </ng-container>


      </div>

      <ng-container *ngIf="(hasError && control.dirty) || error === true">
        <div class="validation-error">
          {{error === true ? errorMessage : message}}
        </div>
      </ng-container>
    </div>`
})
export class FileInputComponent extends BaseInput implements OnInit, OnDestroy {
  @Input() input!: InputViewStructure
  @Input() control!: FormControl
  @Input() formStyle!: FormStyle
  @Input() formGroup !: FormGroup

  defaultErrorMessage = 'Still Uploading'

  progress: number = 0
  fileMessage!: string
  errorMessage!: string
  path: string | null = null
  uploading = false
  error = false

  private progressSubscription: Subscription | undefined | null = null
  private pathSubscription: Subscription | undefined | null = null
  private nameSubscription: Subscription | undefined | null = null

  private uploadTask: UploadResponse | null = null


  ngOnInit(): void {
    this.path = this.input.value as string | null
    this.control.setValue(this.path)
    this.fileMessage = this.input.label
    if (this.path !== null) {
      this.nameSubscription = this.input.fileServer!.getName(this.path).subscribe(
        name => this.fileMessage = name,
        (error: ApiError) => {
          this.errorMessage = "Can't get file name."
          console.log(error)
        }
      )
    }

    this.control.addValidators(this.uploadingValidator())
  }


  ngOnDestroy(): void {
    this.uploadTask?.onCancel()
    this.progressSubscription?.unsubscribe()
    this.pathSubscription?.unsubscribe()
    this.nameSubscription?.unsubscribe()
  }

  onChange(files: FileList | null) {
    if (files != null && files.length > 0) {
      if (this.path !== null) {

        this.startUpload(true)

        this.input.fileServer?.delete(this.path).subscribe(
          () => this.upload(files[0]),
          (error: ApiError) => this.handleError(error)
        )
      } else {
        this.upload(files[0])
      }
    }
  }


  private upload(file: File) {

    this.startUpload(false)
    this.path = null

    this.uploadTask = this.input.fileServer!.push(file)

    this.progressSubscription = this.uploadTask.percentage?.subscribe(
      (progress: number) => this.handleProgress(Math.ceil(progress)),
      (error: ApiError) => console.log(error)
    )

    this.pathSubscription = this.uploadTask.path.subscribe(
      (path: string) => this.completeUpload(path, file),
      (error: ApiError) => this.handleError(error)
    )
  }

  review() {
  }

  cancel() {
    this.ngOnDestroy()
    this.setError("Upload Canceled. Retry")
  }

  private handleProgress(progress: number) {
    this.progress = progress
    this.fileMessage = `Uploading... ${progress}%`
  }

  private startUpload(deleteOldFirst: boolean) {
    this.control.markAsTouched()
    this.control.setValue(this.path)
    this.progress = 0
    this.uploading = true
    this.error = false
    this.fileMessage = deleteOldFirst ? 'Removing old file...' : `Uploading...`

    this.control.updateValueAndValidity()
  }


  private completeUpload(path: string, file: File) {
    this.path = path
    this.control.setValue(this.path)
    this.progress = 0
    this.uploading = false
    this.error = false
    this.fileMessage = file.name
    this.uploadTask = null
    this.control.updateValueAndValidity()

    this.progressSubscription = null
    this.pathSubscription = null
  }

  private handleError(error: ApiError) {
    this.setError("An Error occurred. Retry")
    console.log(error)
  }

  private setError(errorMessage: string) {
    this.path = null
    this.control.setValue(this.path)
    this.progress = 0
    this.uploading = false
    this.error = true
    this.errorMessage = errorMessage
    this.uploadTask = null
    this.control.updateValueAndValidity()
  }


  uploadingValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return this.uploading ? {uploading: {value: control.value}} : null
    }
  }
}
