import {Component, Input} from "@angular/core";
import {InputViewStructure} from "../model/input-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {BaseInput} from "./base-input-component";
import {FormStyle} from "../model/form-style";

@Component({
  selector: 'ng-modular-slider-input',
  template: `
    <div fxFill>
      <div style="width: 100%; min-height: 54px" fxLayout="column">
        <mat-label style="color: VAR(--color-primary-darker)">{{input.label}}</mat-label>
        <mat-slider
          thumbLabel
          [formControl]="control"
          color="primary"
          [min]="input.minLimit"
          [max]="input.maxLimit"
          [displayWith]="formatLabel"
          [value]="value">
        </mat-slider>
        <div style="font-size: 12px!important;">
          <mat-error *ngIf="hasError && control.touched">{{message}}</mat-error>
        </div>
      </div>
    </div>
  `
})
export class SliderComponent extends BaseInput {
  @Input() input!: InputViewStructure
  @Input() control!: FormControl
  @Input() formStyle!: FormStyle
  @Input() formGroup !: FormGroup


  get value(): number {
    return <number>this.input.value
  }

  formatLabel(value: number) {

    if (value >= 1000000) {
      return Math.round(value / 1000000) + 'M';
    }
    if (value >= 1000) {
      return Math.round(value / 1000) + 'K';
    }

    return value;
  }

}
