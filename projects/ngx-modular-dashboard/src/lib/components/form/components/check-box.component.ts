import {Component, Input} from "@angular/core";
import {InputViewStructure} from "../model/input-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {BaseInput} from "./base-input-component";
import {FormStyle} from "../model/form-style";

@Component({
  selector: 'ng-modular-check-box-input',
  template: `
    <div fxFill>
      <div style="width: 100%; min-height: 54px" fxLayout="column">
        <mat-checkbox color="primary" [formControl]="control" [checked]="input.value === true" style="font-size: 16px">
          {{input.label}}
        </mat-checkbox>
        <div style="font-size: 12px!important;">
          <mat-error *ngIf="hasError && control.touched">{{message}}</mat-error>
        </div>
      </div>
    </div>`
})
export class CheckBoxComponent extends BaseInput {
  @Input() input!: InputViewStructure
  @Input() control!: FormControl
  @Input() formStyle!: FormStyle
  @Input() formGroup !: FormGroup
}
