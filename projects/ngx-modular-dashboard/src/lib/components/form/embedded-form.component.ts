import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from "@angular/core";
import {Subscription} from "rxjs";
import {InternalFormComponent} from "./components/internal-form.component";
import {ValidationEvent} from "./model/enums/validation-event-type";
import {ApiError} from "../../infrastructure/api/api-error";
import {EmbeddedFormStructure} from "./model/embedded-form-structure";
import {FormExtractErrors} from "./factory/extract-erros";


@Component({
  selector: 'ng-modular-embedded-form',
  styleUrls: ['./components/error-box.scss'],
  template: `
    <div fxLayout="row wrap">
      <div *ngIf="error!== null" class="error-box" fxFlex="100" fxLayout="row">
        <div class="icon-box" fxFlex="none">
          <mat-icon>sms_failed</mat-icon>
        </div>
        <div class="text-box" fxFlex="grow">
          {{error}}
        </div>
      </div>
      <div fxFlex="100">
        <ng-modular-form
          fxFill #internalFormComponent
          (dataEmitter)="onDataEmitted($event)"
          (stateEmitter)="onStateEmitted($event)"
          [inputs]="structure.inputs"
          [events]="structure.submitData"
          [crossValidators]="structure.crossValidators ?? []"
          [formStyle]="structure.style">
        </ng-modular-form>
      </div>
    </div>
  `
})
export class EmbeddedFormComponent implements OnInit, OnDestroy {

  @Input() structure !: EmbeddedFormStructure
  @Output() successEmitter: EventEmitter<void> = new EventEmitter<void>()
  @Output() apiErrorEmitter: EventEmitter<ApiError> = new EventEmitter<ApiError>()

  invalid: boolean = true
  loading: boolean = false
  error: string | null = null


  private submitSubscription: Subscription | null = null

  @ViewChild('internalFormComponent') formComponent!: InternalFormComponent


  ngOnInit(): void {

  }


  onStateEmitted(event: ValidationEvent): void {
    this.invalid = event != ValidationEvent.VALID
    if (event == ValidationEvent.INVALID_ERROR) {
      const errors = FormExtractErrors.extract({
        form: this.formComponent.form,
        crossValidators: this.structure.crossValidators !== undefined ? this.structure.crossValidators : [],
        inputs: this.structure.inputs
      });

      if (errors.length > 0) {
        this.error = errors[0]
      }
    } else if (event == ValidationEvent.VALID) {
      this.error = null
    }

    if (this.error !== null) {
      this.apiErrorEmitter.next(new ApiError(500, 'client/validation', this.error))
    }
  }

  onDataEmitted(data: any): void {
    this.loading = true

    this.submitSubscription = this.structure.onDataSubmitted(data, this.formComponent.form).subscribe({
      next: _ => this.handleSuccess(),
      error: (error: ApiError) => this.handleError(error)
    })
  }

  private handleSuccess() {
    this.loading = false
    this.error = null
    this.successEmitter.next()
  }

  private handleError(error: ApiError) {
    this.loading = false
    this.error = error.errorMessage
    this.apiErrorEmitter.next(error)
  }

  ngOnDestroy(): void {
    this.submitSubscription?.unsubscribe()
  }
}
