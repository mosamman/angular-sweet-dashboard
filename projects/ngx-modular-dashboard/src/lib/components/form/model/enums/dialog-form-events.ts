export enum DialogFormEvent {
  CANCELED = "CANCELED",
  SUBMITTED = "SUBMITTED",
}
