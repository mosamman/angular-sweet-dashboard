export enum ValidationEvent {
  VALID = "VALID",
  INVALID = "INVALID",
  INVALID_ERROR = "INVALID_ERROR"
}
