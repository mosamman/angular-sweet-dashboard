import {AsyncValidatorFn, ValidatorFn} from "@angular/forms";

export interface InputValidator {
  id: string,
  message: string,
  validatorFun?: ValidatorFn
  asyncValidatorFun?: AsyncValidatorFn
}
