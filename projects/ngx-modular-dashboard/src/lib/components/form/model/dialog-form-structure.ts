import {InputViewStructure} from "./input-structure";
import {Observable} from "rxjs";
import {FormGroup} from "@angular/forms";
import {FormStyle} from "./form-style";
import {InputValidator} from "./input-validator";

export interface DialogFormStructure {
  title: string
  cancelButton: string
  submitButton: string
  style: FormStyle,
  inputs: InputViewStructure[]
  crossValidators?: InputValidator[]
  onDataSubmitted: (request: any, form?: FormGroup) => Observable<any>
}
