import {Observable} from "rxjs";
import {Moment} from "moment";
import {InputType} from "./enums/input-type";
import {InputListItem} from "./input-list-item";
import {InputValidator} from "./input-validator";
import {SearchResult} from "./search-result";
import {BaseFileServer} from "../../../infrastructure/file-server/base-file-server";
import {Dimensions} from "../../common/dimensions";

export interface InputViewStructure {
  id: string
  value: string | number | number[] | boolean | null
  label: string
  longLabel: string
  type: InputType
  list?: InputListItem[]
  asyncList?: Observable<InputListItem[]>
  validators: InputValidator[]
  display: (formData: any) => boolean
  autoComplete?: (query: string | null) => Observable<SearchResult[]>
  maxDate?: Moment | null
  minDate?: Moment | null
  dimensions: Dimensions
  fileServer?: BaseFileServer
  fileType?: string
  forgetPassword?: () => void
  showForgetPassword?: boolean
  multiSelect?: boolean
  minLimit?: number
  maxLimit?: number
}
