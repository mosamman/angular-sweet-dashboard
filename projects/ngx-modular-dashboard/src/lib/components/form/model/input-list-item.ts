export interface InputListItem {
  value: string
  name: string
}
