export interface SearchResult {
  id: string
  value: string
}
