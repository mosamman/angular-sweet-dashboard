import {DynamicRowActionStructure, RowActionStructure, RowActionViewStructure} from "../model/row-action-structure";
import {HeaderActionStructure, HeaderActionViewStructure} from "../model/header-action-structure";
import {
  DetailsHeaderActionStructure,
  DetailsHeaderActionViewStructure
} from "../../details/model/details-header-action-structure";

// noinspection JSUnusedGlobalSymbols
export class ActionFactory {
  static actionButton<TYPE>(structure: RowActionStructure<TYPE>): RowActionViewStructure<TYPE> {
    let defaultDisplay = () => true
    return {
      name: () => structure.name,
      type: () => structure.type,
      display: structure.display != undefined ? structure.display : defaultDisplay,
      callback : structure.callback
    }
  }

  static actionDynamicButton<TYPE>(structure: DynamicRowActionStructure<TYPE>): RowActionViewStructure<TYPE> {
    let defaultDisplay = () => true
    return {
      name: structure.name,
      type: structure.type,
      display: structure.display != undefined ? structure.display : defaultDisplay,
      callback : structure.callback
    }
  }


  static headerActionButton(structure: HeaderActionStructure): HeaderActionViewStructure {
    let defaultDisplay = () => true
    return {
      name: structure.name,
      display: structure.display != undefined ? structure.display : defaultDisplay,
      callback : structure.callback
    }
  }


  static detailsActionButton(structure: DetailsHeaderActionStructure): DetailsHeaderActionViewStructure {
    let defaultDisplay = () => true
    return {
      name: structure.name,
      icon: structure.icon != undefined ? structure.icon : null,
      display: structure.display != undefined ? structure.display : defaultDisplay,
      callback : structure.callback
    }
  }

}
