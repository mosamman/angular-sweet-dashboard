
export enum ColumnType {
  PLAIN = "PLAIN",
  ACTION = "ACTION",
  SELECT = "SELECT",
}
