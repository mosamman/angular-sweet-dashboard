import {SearchFilterType} from "./enums/search-filter-type";

export interface SearchBag {
  filters?: FilterBox[]
  sorts?: SortBox[]
  pageBox?: PaginationBox
}

export interface FilterBox {
  readonly id: string
  readonly value: string
  readonly operator: string
  readonly type: SearchFilterType
}

export interface SortBox {
  sortField: string
  sortDirection: string
}

export interface PaginationBox {
  page: number
  pageSize: number
}
