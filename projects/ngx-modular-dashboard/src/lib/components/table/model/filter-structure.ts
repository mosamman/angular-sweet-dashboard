import {Observable} from "rxjs";
import {FilterType} from "./enums/filter-type";
import {Operator} from "./enums/operator";
import {FilterListItem} from "./filter-list-item";

export interface FilterStructure {
  readonly filterId: string
  readonly operatorId: string
  readonly placeHolder: string
  readonly type: FilterType
  readonly operators: Operator[]
  list?: FilterListItem[]
  readonly asyncList?: Observable<FilterListItem[]>
}
