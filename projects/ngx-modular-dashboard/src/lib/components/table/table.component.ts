import {
  AfterViewInit,
  Component, Input, OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core'
import {MatMultiSort, MatMultiSortTableDataSource, TableData} from "ngx-mat-multi-sort"
import {FormBuilder, FormGroup} from "@angular/forms"
import {TableResponse} from "./model/table-response"
import {ApiError} from "../../infrastructure/api/api-error"
import {Subscription} from "rxjs"
import {TableStructure} from "./model/table-structure";
import {TableColumnStructure} from "./model/table-column-structure";
import {ColumnType} from "./model/enums/column-type";
import {FilterStructure} from "./model/filter-structure";
import {FilterType} from "./model/enums/filter-type";
import {SearchBagFactory} from "./factory/search-bag-factory";
import {ResizedEvent} from "./resize.directive";
import {ArtType} from "../common/art-type";


@Component({
  selector: 'ng-modular-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss', '../header.scss'],
})
export class TableComponent implements OnInit, AfterViewInit, OnDestroy {

  ART_TYPE = ArtType;
  @Input() structure!: TableStructure
  @ViewChild(MatMultiSort, {static: false}) sort !: MatMultiSort

  loading = false
  firstLoad = false
  error: string | null = null

  form!: FormGroup
  table!: TableData<any>

  private table_width: number = 1
  private table_height: number = 1
  private eventsSubscription: Subscription | null = null
  private tableNextSubscription: Subscription | null = null
  private tableSortSubscription: Subscription | null = null
  private tablePreviousSubscription: Subscription | null = null
  private tableSizeSubscription: Subscription | null = null
  private dataSubscription: Subscription | null = null

  constructor(
    private formBuilder: FormBuilder
  ) {

  }


  ngOnInit() {
    this.initializeForm()
    this.initializeTable()
    this.eventsSubscription = this.structure?.reloadEvent?.subscribe(() => this.reloadTable()) ?? null
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.table.dataSource = new MatMultiSortTableDataSource(this.sort, false)
      this.reloadTable()
    }, 0)
  }

  ngOnDestroy() {
    this.eventsSubscription?.unsubscribe()
    this.tableNextSubscription?.unsubscribe()
    this.tableSortSubscription?.unsubscribe()
    this.tablePreviousSubscription?.unsubscribe()
    this.tableSizeSubscription?.unsubscribe()
    this.dataSubscription?.unsubscribe()
  }

  onResized(event: ResizedEvent) {
    this.onTableSizeChange(event.newRect.width, event.newRect.height)
  }


  private onTableSizeChange(width: number, height: number) {
    this.table_width = width
    this.table_height = height

    const allColumns = this.structure.columns

    const allColumnsWidth = allColumns.reduce((width: number, col: TableColumnStructure) => col.width + width, 0)

    if (allColumnsWidth < width) {
      this.structure.columns.forEach(col => {
        col.displayWidth = col.width + ((width - allColumnsWidth) / allColumns.length)
      })
    } else {
      this.structure.columns.forEach(col => {
        col.displayWidth = col.width
      })
    }
  }

  onApplyFilter() {
    this.reloadTable()
  }


  get loadingWidth() {
    return `${this.table_width}px`
  }

  get loadingHeight() {
    return `${this.table_height / 4}px`
  }


  columnType(column: TableColumnStructure): string {
    switch (column.type) {
      case ColumnType.PLAIN:
        return 'PLAIN'
      case ColumnType.SELECT:
        return 'LIST'
      case ColumnType.ACTION:
        return 'ACTION'
    }
  }


  filterType(filter: FilterStructure): string {
    switch (filter.type) {
      case FilterType.DATE_RANG:
        return 'DATE'
      case FilterType.TEXT:
      case FilterType.INTEGER:
      case FilterType.FLOAT:
        return 'TYPE'
      case FilterType.SELECT:
        return 'SELECT'
      case FilterType.ASYNC_SELECT:
        return 'ASYNC_SELECT'
      case FilterType.NONE:
        return 'NONE'
    }
  }


  columnWidth(column: TableColumnStructure): string {
    return `${column.displayWidth}px`
  }

  get filtersColumns(): string[] {
    return this.structure.columns.map(column => column.filter.filterId)
  }

  get hasFilters(): boolean {
    return this.structure.columns.filter(col => col.filter.type !== FilterType.NONE).length !== 0
  }

  get displayed(): string[] {
    return this.structure.columns.map(column => column.id)
  }


  private initializeTable() {
    this.table = new TableData<any>(
      this.structure.columns.map(column => {
        return {
          id: column.id,
          name: column.name,
          isActive: column.active
        }
      }),
      {
        defaultSortParams: this.structure.defaultSortParams,
        defaultSortDirs: this.structure.defaultSortDirs,
        totalElements: this.structure.totalElements,
      },
    )

    this.tableNextSubscription = this.table.nextObservable.subscribe(() => this.reloadTable())
    this.tableSortSubscription = this.table.sortObservable.subscribe(() => this.reloadTable())
    this.tablePreviousSubscription = this.table.previousObservable.subscribe(() => this.reloadTable())
    this.tableSizeSubscription = this.table.sizeObservable.subscribe(() => this.reloadTable())
  }


  private initializeForm() {
    let group = this.formBuilder.group({})
    this.structure.columns.forEach(column => {
      switch (column.filter.type) {
        case FilterType.NONE:
          break
        case FilterType.SELECT:
        case FilterType.ASYNC_SELECT:
          const listControl = this.formBuilder.control([])
          group.addControl(column.filter.filterId, listControl)
          break
        case FilterType.TEXT:
        case FilterType.INTEGER:
        case FilterType.FLOAT:
          const operatorControl = this.formBuilder.control(column.filter.operators[0])
          group.addControl(column.filter.operatorId, operatorControl)
          const typeControl = this.formBuilder.control('')
          group.addControl(column.filter.filterId, typeControl)
          break
        case FilterType.DATE_RANG:
          const start = this.formBuilder.control('')
          group.addControl(column.filter.filterId, start)
          const end = this.formBuilder.control('')
          group.addControl(column.filter.operatorId, end)
          break
      }
    })

    this.form = group
  }

  reloadTable() {
    this.loading = true
    this.dataSubscription = this.structure.loadData(SearchBagFactory.extractSearchBag(this)).subscribe({
      next: response => this.handleSuccess(response),
      error: (error: ApiError) => this.handleError(error)
    })
  }

  resetFilters() {
    this.initializeForm()
    this.reloadTable()
  }


  private handleSuccess(response: TableResponse<any>) {
    setTimeout(() => {
      this.loading = false
      this.error = null
    }, 500)
    this.firstLoad = true
    this.table.totalElements = response.count
    this.table.pageIndex = response.page
    this.table.pageSize = response.pageSize
    this.table.data = response.data
  }

  private handleError(error: ApiError) {
    this.loading = false
    this.firstLoad = true
    this.error = error.errorMessage
    console.log(error)
  }

}
