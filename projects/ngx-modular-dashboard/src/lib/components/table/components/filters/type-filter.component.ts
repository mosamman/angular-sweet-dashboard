import {Component, EventEmitter, Input, Output} from "@angular/core";
import {FilterStructure} from "../../model/filter-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {Operator} from "../../model/enums/operator";


@Component({
  selector: 'ng-modular-type-filter',
  styleUrls: ['../table.cell.scss'],
  template: `

    <input type="hidden" [formControl]="operatorControl" [id]="filter.operatorId">

    <mat-form-field fxFlex="100" ngDefaultControl [id]="filter.filterId" appearance="outline" color="primary"
                    [formControl]="control" class="type-field">
      <label><input (keyup)="applyFilter.emit()" matInput></label>
      <span class="filter-icon" matSuffix [matMenuTriggerFor]="filterMenu">{{operatorIcon(operatorControl.value)}}</span>

      <mat-menu #filterMenu="matMenu">
        <mat-option class="filter-option" *ngFor="let operator of filter.operators" (click)="filterChanged(operator)" fxLayout="row">
          <div class="filter-option-text" fxFlex="grow">{{operatorText(operator)}}</div>
          <div class="filter-option-icon" fxFlex="none">{{operatorIcon(operator)}}</div>
        </mat-option>
      </mat-menu>
    </mat-form-field>`
})
export class TypeFilter {
  @Input() filter!: FilterStructure
  @Output() applyFilter: EventEmitter<any> = new EventEmitter<any>()
  @Input() form!: FormGroup

  get control(): FormControl {
    return this.form.get(this.filter.filterId) as FormControl
  }

  get operatorControl(): FormControl {
    return this.form.get(this.filter.operatorId) as FormControl
  }

  filterChanged(operator: Operator) {
    this.operatorControl.setValue(operator.toString())
    this.applyFilter.emit()
  }

  operatorIcon(action: string | null) {
    switch (action) {
      case Operator.EQUAL.toString():
        return "="
      case Operator.NOT_EQUAL.toString():
        return "!="
      case Operator.LIKE.toString():
        return "~"
      case Operator.GREATER_THEN.toString():
        return ">"
      case Operator.LESS_THEN.toString():
        return "<"
      case Operator.GREATER_THEN_OR_EQUAL.toString():
        return ">="
      case Operator.LESS_THEN_OR_EQUAL.toString():
        return "<="
      case Operator.IN.toString():
        return "()"
      case Operator.IN_RANG.toString():
        return "<>"
      default:
        return "~"
    }
  }

  operatorText(action: string | null) {
    switch (action) {
      case Operator.EQUAL.toString():
        return "Equal"
      case Operator.NOT_EQUAL.toString():
        return "Not Equal"
      case Operator.LIKE.toString():
        return "Like"
      case Operator.GREATER_THEN.toString():
        return "Greater Then"
      case Operator.LESS_THEN.toString():
        return "Less Then"
      case Operator.GREATER_THEN_OR_EQUAL.toString():
        return "Greater Then Or Equal"
      case Operator.LESS_THEN_OR_EQUAL.toString():
        return "Less Then Or Equal"
      case Operator.IN.toString():
        return "In List"
      case Operator.IN_RANG.toString():
        return "In Range"
      default:
        return ""
    }
  }

}
