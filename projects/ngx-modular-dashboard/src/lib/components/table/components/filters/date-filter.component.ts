import {Component, EventEmitter, Input, Output} from "@angular/core";
import {FilterStructure} from "../../model/filter-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {isMoment} from "moment";




@Component({
  selector: 'ng-modular-date-filter',
  styleUrls: ['../table.cell.scss'],
  template: `
    <mat-form-field fxFill ngDefaultControl [id]="filter.filterId" appearance="outline" color="primary">
      <mat-date-range-input [rangePicker]="picker">
        <input matStartDate [formControl]="startControl" placeholder="Start">
        <input matEndDate [formControl]="endControl" placeholder="End" (dateInput)="emit()">
      </mat-date-range-input>
      <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>

      <mat-date-range-picker #picker></mat-date-range-picker>
    </mat-form-field>
  `
})
export class DateFilter {
  @Input() filter!: FilterStructure
  @Input() form!: FormGroup
  @Output() applyFilter: EventEmitter<any> = new EventEmitter<any>()

  get startControl(): FormControl {
    return this.form.get(this.filter.filterId) as FormControl
  }

  get endControl(): FormControl {
    return this.form.get(this.filter.operatorId) as FormControl
  }

  emit() {
    let startValue = this.startControl.value
    let endValue = this.endControl.value
    if (startValue !== null && endValue !== null && isMoment(startValue) && isMoment(endValue))
      this.applyFilter.emit()
  }
}
