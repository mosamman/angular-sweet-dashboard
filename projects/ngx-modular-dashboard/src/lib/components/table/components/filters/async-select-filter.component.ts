import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from "@angular/core";
import {FilterStructure} from "../../model/filter-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {FilterListItem} from "../../model/filter-list-item";
import {ApiError} from "../../../../infrastructure/api/api-error";
import {Subscription} from "rxjs";


@Component({
  selector: 'ng-modular-async-select-filter',
  styleUrls: ['../table.cell.scss'],
  template: `
    <mat-form-field fxFill ngDefaultControl [id]="filter.filterId" appearance="outline" color="primary">
      <mat-select (selectionChange)="applyFilter.emit()" multiple [formControl]="control">
        <mat-option *ngFor="let item of asyncList" [value]="item.value">{{item.name}}</mat-option>
      </mat-select>
      <mat-spinner *ngIf="loading" matSuffix [diameter]="18" style="float: right; margin-left: 8px"></mat-spinner>
    </mat-form-field>`
})
export class AsyncSelectFilter implements OnInit, OnDestroy {
  @Input() filter!: FilterStructure
  @Output() applyFilter: EventEmitter<any> = new EventEmitter<any>()
  @Input() form!: FormGroup

  asyncList: FilterListItem[] = []
  loading = true
  error = false//TODO implement error view and retry action

  private asyncListSubscription: Subscription | null = null


  get control(): FormControl {
    return this.form.get(this.filter.filterId) as FormControl
  }

  ngOnInit(): void {
    if (this.filter.asyncList !== undefined)
      this.asyncListSubscription = this.filter.asyncList.subscribe({
        next: list => {
          this.asyncList = list
          this.filter.list = list
          this.error = false
          this.loading = false
        },
        error: (error: ApiError) => {

          this.error = true
          this.loading = false
        }
      })

  }

  ngOnDestroy(): void {
    this.asyncListSubscription?.unsubscribe()
  }
}
