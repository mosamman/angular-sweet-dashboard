import {Component, EventEmitter, Input, Output} from "@angular/core";
import {FilterStructure} from "../../model/filter-structure";
import {FormControl, FormGroup} from "@angular/forms";


@Component({
  selector: 'ng-modular-select-filter',
  styleUrls: ['../table.cell.scss'],
  template: `
    <mat-form-field fxFill ngDefaultControl [id]="filter.filterId" appearance="outline" color="primary">
      <mat-select (selectionChange)="applyFilter.emit()" multiple [formControl]="control">
        <mat-option *ngFor="let item of filter.list" [value]="item.value">{{item.name}}</mat-option>
      </mat-select>
    </mat-form-field>`
})
export class SelectFilter {
  @Input() filter!: FilterStructure
  @Output() applyFilter: EventEmitter<any> = new EventEmitter<any>()
  @Input() form!: FormGroup

  get control(): FormControl {
    return this.form.get(this.filter.filterId) as FormControl
  }
}
