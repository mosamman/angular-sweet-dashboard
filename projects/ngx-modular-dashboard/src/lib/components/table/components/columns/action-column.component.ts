import {Component, Input, OnInit} from "@angular/core";
import {TableColumnStructure} from "../../model/table-column-structure";
import {RowActionViewStructure} from "../../model/row-action-structure";
import {ThemePalette} from "@angular/material/core";
import {RowActionType} from "../../model/enums/row-action-type";

@Component({
  selector: 'ng-modular-actions-column',
  styleUrls: ['../table.cell.scss'],
  template: `
    <div class="cell-box">
      <ng-container *ngIf="activeActions.length < 1 else SINGLE">
        <button mat-stroked-button type="button" disabled>No Action</button>
      </ng-container>
      <ng-template #SINGLE>
        <ng-container *ngIf="activeActions.length === 1 else MULTI">
          <button (click)="onlyAction.callback(row)" mat-stroked-button type="button"
                  [color]="actionButtonColor(onlyAction)">{{actionButtonText(onlyAction)}}</button>
        </ng-container>
      </ng-template>
      <ng-template #MULTI>
        <button mat-stroked-button type="button" [matMenuTriggerFor]="menu">{{column.name}}</button>
      </ng-template>
      <mat-menu #menu>
        <ng-container *ngFor="let action of activeActions">
          <button (click)="action.callback(row)" mat-stroked-button
                  [color]="actionButtonColor(action)">{{actionButtonText(action)}}</button>
        </ng-container>
      </mat-menu>
    </div>`
})
export class ActionsColumn implements OnInit {
  @Input() row!: any
  @Input() column!: TableColumnStructure

  activeActions: RowActionViewStructure<any>[] = []

  actionButtonColor(action: RowActionViewStructure<any>): ThemePalette {
    return action.type(this.row) === RowActionType.WARNING ? "warn" : "primary";
  }

  actionButtonText(action: RowActionViewStructure<any>): string {
    return action.name(this.row) !== null ? action.name(this.row)! : ""
  }

  get onlyAction() {
    return this.activeActions[0]
  }

  ngOnInit(): void {
    this.activeActions = this.column.actions.filter(action => action.display(this.row))
  }
}
