import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {TableColumnStructure} from "../../model/table-column-structure";
import {Directive, ViewContainerRef} from '@angular/core';

export abstract class PlainColumnContent {
  data!: string | number | boolean | {}
}


@Directive({
  selector: '[plainContentAnchor]'
})
export class PlainColumnContentDirective {

  constructor(public viewContainerRef: ViewContainerRef) {
  }

}


@Component({
  selector: 'ng-modular-plain-column',
  styleUrls: ['../table.cell.scss'],
  template: `
    <div (click)="clickable ? column?.onClick(row) : nothing()" class="cell-box">
      <ng-template plainContentAnchor></ng-template>
      <div *ngIf="column.customContent === undefined" class="plain-column">
        {{row[column.id]}}
      </div>
    </div>`
})
export class PlainColumn implements OnInit {
  @Input() row!: any
  @Input() column!: TableColumnStructure

  loaded: boolean = false

  @ViewChild(PlainColumnContentDirective, {static: true}) contentHost!: PlainColumnContentDirective;

  ngOnInit(): void {
    const viewContainerRef = this.contentHost.viewContainerRef;
    viewContainerRef.clear();
    if (this.column.customContent !== undefined && this.column.customContentSelector !== undefined) {
      const componentRef = viewContainerRef.createComponent(this.column.customContent);
      componentRef.instance.data = this.column.customContentSelector(this.row);
      this.loaded = true
    }
  }

  get clickable(): boolean {
    return this.column.onClick !== undefined
  }

  nothing() {
  }

}

