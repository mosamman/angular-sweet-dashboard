import {ArtType} from "./art-type";

export interface Art {
  artType: ArtType
  art: string
  text: string
}
