
export interface FlexDefinition {
  lg: number
  md: number
  sm: number
  xs: number
}
