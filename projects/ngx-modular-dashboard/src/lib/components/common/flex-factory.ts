import {Dimensions} from "./dimensions";

export class FlexFactory {

  public static fixedWidth(width: number): Dimensions {
    return {width: width}
  }


  public static fixedFlex(flex: number): Dimensions {
    return {
      flex: {lg: flex, md: flex, sm: flex, xs: flex}
    }
  }


  public static defaultFlex(): Dimensions {
    return {
      flex: {lg: 25.0, md: 33.3, sm: 50.0, xs: 100.0}
    }
  }


  public static flex(flex: { lg: number, md?: number, sm?: number, xs?: number }): Dimensions {
    return {
      flex: {
        lg: flex.lg,
        md: flex.md ?? flex.lg,
        sm: flex.sm ?? (flex.md ?? flex.lg),
        xs: flex.xs ?? (flex.sm ?? (flex.md ?? flex.lg))
      }
    }
  }


}
