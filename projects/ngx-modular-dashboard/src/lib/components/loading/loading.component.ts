import {Component, Input} from '@angular/core';

@Component({
  selector: 'ng-modular-loading',
  styleUrls: ['./loading.scss'],
  template: `
    <div fxLayout="row">
      <div fxFill>
        <ng-container *ngIf="loading">
          <div class="loading-box">
            <ng-container *ngIf="loadingAnimation != undefined else SPINNER">
              <ng-lottie [options]="{path: loadingAnimation}" height="120px" width="100%"></ng-lottie>
            </ng-container>
            <ng-template #SPINNER>
              <div class="spinner-box">
                <div class="spinner-row">
                  <mat-spinner mode="indeterminate" diameter="40"></mat-spinner>
                </div>
              </div>
            </ng-template>
          </div>
        </ng-container>
        <ng-container *ngIf="empty">
          <div class="empty">
            <div class="empty-image-box">
              <ng-container *ngIf="emptyImage !== undefined">
                <img class="empty-image" [src]="emptyImage" alt="">
              </ng-container>
            </div>
            <div class="empty-text">
              {{emptyText !== undefined ? emptyText : 'Nothing here. Just chilling.'}}
            </div>
          </div>
        </ng-container>
      </div>
    </div>
  `,
})
export class LoadingBox {
  @Input() loading = false
  @Input() empty = false
  @Input() loadingAnimation: string | undefined
  @Input() emptyImage: string | undefined
  @Input() emptyText: string | undefined
}
